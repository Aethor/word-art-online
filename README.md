# Word Art Online


# Cloning

Word Art Online depends on [GNA](https://gitlab.com/Aethor/gna), which is included as a git submodule. To clone the project, including GNA, run :

> git clone --recurse-submodules https://gitlab.com/Aethor/word-art-online.git

or :

> git clone --recurse-submodules git@gitlab.com:Aethor/word-art-online.git
