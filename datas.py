from typing import Generator, List, Tuple
import glob
import re
import logging

import torch
from transformers import GPT2Tokenizer
from bs4 import BeautifulSoup
from tqdm import tqdm

logging.getLogger("transformers.tokenization_utils").setLevel(logging.ERROR)


def extract_file_id(filename: str) -> int:
    id_str = re.search(r"messages([0-9]*)\.html", filename).group(1)
    if id_str == "":
        return 0
    return int(id_str)


def pad_tensor_1d(tens: torch.Tensor, max_len: int) -> Tuple[torch.Tensor]:
    """
    :return: padded_tensor, attention_mask
    """
    if tens.shape[0] >= max_len:
        return tens, torch.ones(tens.shape[0])
    padded_tensor = torch.zeros(max_len)
    padded_tensor[: tens.shape[0]] = tens

    attention_mask = torch.zeros(max_len)
    attention_mask[: tens.shape[0]] = torch.ones(tens.shape[0])

    return padded_tensor, attention_mask


class Message:
    def __init__(self, user: str, raw_text: str, tokenizer):
        self.user = user
        self.text = f"{user} : {raw_text}"
        self.tokens = tokenizer.tokenize(self.text)

    def __len__(self) -> int:
        return len(self.tokens)

    def __str__(self) -> str:
        return self.text


def messages_from_str(input_str: str, tokenizer) -> List[Message]:
    messages = []
    for line in input_str.split("\n"):
        user, raw_text = line.split(" : ")
        messages.append(Message(user, raw_text, tokenizer))
    return messages


def batches_nb(sequences: List[List[Message]], batch_size: int) -> int:
    return len(sequences) // batch_size


def batches(
    sequences: List[List[Message]], tokenizer, batch_size: int
) -> Generator[Tuple[torch.Tensor], None, None]:
    """
    :yield: a tuple of torch.Tensor
        input_ids       (batch_size, max_seq_len)
        attention_mask  (batch_size, max_seq_len)
        token_type_ids  (batch_size, max_seq_len)
        target_ids      (batch_size, max_seq_len)
    """
    for i in range(batches_nb(sequences, batch_size)):

        input_ids_list = []
        token_type_ids_list = []
        target_ids_list = []

        for j in range(batch_size):

            cur_seq = sequences[i * batch_size + j]

            input_ids = []
            if not isinstance(tokenizer, GPT2Tokenizer):
                input_ids.append(tokenizer.bos_token)
            for message in cur_seq[:-1]:
                input_ids += message.tokens
            input_ids += [tokenizer.sep_token, tokenizer.sep_token]
            token_type_ids = [0] * len(input_ids)
            input_ids += cur_seq[-1].tokens
            token_type_ids += [1] * (len(input_ids) - len(token_type_ids))

            target_ids = []
            if not isinstance(tokenizer, GPT2Tokenizer):
                target_ids.append(tokenizer.bos_token)
            for message in cur_seq[:-1]:
                target_ids += [tokenizer.pad_token] * len(message.tokens)
            target_ids += [tokenizer.sep_token, tokenizer.sep_token]
            target_ids += cur_seq[-1].tokens[1:]
            target_ids.append(tokenizer.eos_token)

            token_type_ids_list.append(token_type_ids)
            input_ids_list.append(tokenizer.encode(input_ids, add_special_tokens=False))
            target_ids_list.append(
                tokenizer.encode(target_ids, add_special_tokens=False)
            )

        max_seq_len = max([len(inp) for inp in input_ids_list])

        input_ids_tens = torch.zeros(batch_size, max_seq_len, dtype=torch.long)
        attention_mask_tens = torch.zeros(batch_size, max_seq_len, dtype=torch.long)
        token_type_ids_tens = torch.zeros(batch_size, max_seq_len, dtype=torch.long)
        target_ids_tens = torch.zeros(batch_size, max_seq_len, dtype=torch.long)

        for j in range(batch_size):
            input_ids_tens[j], attention_mask_tens[j] = pad_tensor_1d(
                torch.tensor(input_ids_list[j], dtype=torch.long), max_seq_len
            )
            target_ids_tens[j], _ = pad_tensor_1d(
                torch.tensor(target_ids_list[j], dtype=torch.long), max_seq_len
            )
            token_type_ids_tens[j] = torch.tensor(
                token_type_ids_list[j]
                + [1] * (input_ids_tens[j].shape[0] - len(token_type_ids_list[j])),
                dtype=torch.long,
            )

        yield input_ids_tens, attention_mask_tens, token_type_ids_tens, target_ids_tens


class Dataset:
    def __init__(self, path: str, tokenizer, datas_usage_ratio: float = 1.0):
        path = path.rstrip("/")

        self.users = {}
        self.messages = []

        self.message_filepaths = sorted(
            glob.glob(f"{path}/messages*.html"), key=lambda e: extract_file_id(e)
        )

        print("loading datas...")
        for filepath in tqdm(
            self.message_filepaths[
                : int(len(self.message_filepaths) * datas_usage_ratio)
            ]
        ):

            with open(filepath) as stream:
                soup = BeautifulSoup(stream.read(), "html.parser")
            raw_messages = soup.findAll(
                lambda tag: tag.name == "div" and tag["class"] == ["body"]
            )

            for raw_message in raw_messages:

                text_nodes = raw_message.findChildren(
                    lambda tag: "class" in tag.attrs and tag["class"] == ["text"]
                )
                if len(text_nodes) == 0:
                    continue
                text = text_nodes[0].text.strip(" ").strip("\n")

                user_nodes = raw_message.findChildren(
                    lambda tag: "class" in tag.attrs and tag["class"] == ["from_name"]
                )
                if len(user_nodes) == 0:
                    continue
                user = user_nodes[0].text.strip(" ").strip("\n").strip(" ")

                self.messages.append(Message(user, text, tokenizer))
                if not user in self.users:
                    self.users[user] = 0
                else:
                    self.users[user] += 1

    def message_context(
        self, message_id: int, max_messages_nb, max_context_len: int
    ) -> List[str]:
        if message_id > len(self.messages) + 1:
            raise Exception

        context_len = 0
        context: List[Message] = list()

        for i in range(message_id, -1, -1):

            cur_message = self.messages[i]

            if context_len + len(cur_message) > max_context_len:
                break

            if len(context) >= max_messages_nb:
                break

            context.append(cur_message)
            context_len += len(cur_message)

        context.reverse()

        return context

    def sequences(
        self, target_user: str, max_messages_nb: int, max_context_len: int
    ) -> List[str]:
        sequences: List[List[Message]] = list()
        for i, message in enumerate(self.messages):
            if not message.user == target_user:
                continue
            sequences.append(self.message_context(i, max_messages_nb, max_context_len))
        return [seq for seq in sequences if len(seq) > 0]
