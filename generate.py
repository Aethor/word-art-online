import sys
from typing import List

import torch
from transformers import CamembertTokenizer, GPT2Tokenizer

from gna.gna.config import Config
from model import GPT2UserImpersonator, CamembertUserImpersonator
from datas import Message


def generate(
    model: torch.nn.Module,
    user_prompt: str,
    context: List[Message],
    device: torch.device,
    tokenizer,
    max_answer_size: int,
    nucleus_size: int,
) -> str:
    model.to(device)
    model.eval()

    input_ids = []
    if not isinstance(tokenizer, GPT2Tokenizer):
        input_ids.append(tokenizer.bos_token)
    for message in context:
        input_ids += message.tokens
    input_ids += [tokenizer.sep_token, tokenizer.sep_token]
    token_type_ids = [0] * len(input_ids)
    user_prompt = f"{user} : "
    input_ids += tokenizer.tokenize(user_prompt)
    token_type_ids += [1] * (len(input_ids) - len(token_type_ids))

    input_ids = (
        torch.tensor(
            tokenizer.encode(input_ids, add_special_tokens=False), dtype=torch.long
        )
        .unsqueeze(0)
        .to(device)
    )
    token_type_ids = (
        torch.tensor(token_type_ids, dtype=torch.long).unsqueeze(0).to(device)
    )

    with torch.no_grad():

        answer = []
        past = None

        while len(answer) < max_answer_size:
            if len(answer) > 0 and tokenizer.decode(answer[-1]) == tokenizer.eos_token:
                break

            if isinstance(model, CamembertUserImpersonator):
                raw_outputs = model(input_ids, None)[0]
            elif isinstance(model, GPT2UserImpersonator):
                raw_outputs, past = model(input_ids, None, token_type_ids, past)
            else:
                raise Exception(f"unknown model type : {type(model)}")
            outputs = torch.softmax(raw_outputs[0][-1], 0)
            top_choices = torch.topk(outputs, nucleus_size)
            last_token_id = top_choices.indices[
                torch.multinomial(top_choices.values, 1)
            ]
            answer.append(last_token_id)

            input_ids = torch.cat((input_ids[0], last_token_id)).unsqueeze(0)
            token_type_ids = torch.cat(
                (token_type_ids[0], torch.tensor([1]).to(device))
            ).unsqueeze(0)

    return tokenizer.decode(answer)


if __name__ == "__main__":
    config = Config("./configs/definitions/generate.json", sys.argv[1:])

    print(f"running with config :\n {str(config)}")

    model_dir_path = "./models/" + config["model_name"]

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    model_checkpoint = torch.load(model_dir_path + "/model.pth", map_location=device)

    model_type = model_checkpoint["model_type"]
    known_users = model_checkpoint["users"]
    target_user = model_checkpoint["target_user"]

    if model_type == "GPT2UserImpersonator":
        model = GPT2UserImpersonator()
        tokenizer = GPT2Tokenizer.from_pretrained("gpt2")
        tokenizer.sep_token = tokenizer.eos_token
        tokenizer.pad_token = tokenizer.eos_token
    elif model_type == "CamembertUserImpersonator":
        model = CamembertUserImpersonator()
        tokenizer = CamembertTokenizer.from_pretrained("camembert-base")
    else:
        raise Exception(
            "transformer model type : {}".format(config["transformer_model_type"])
        )
    model.load_state_dict(model_checkpoint["model_state"], strict=False)

    bot_prompt = target_user + " : "

    print("Choose your username by typing its id : ")
    print([(user_i, user) for user_i, user in enumerate(known_users)])
    user = None
    while user is None:
        user_answer = int(input("id>"))
        if user_answer >= 0 and user_answer < len(known_users):
            user = known_users[user_answer]

    user_answer = None
    context: List[Message] = []
    while True:
        user_answer = input(user + " : ")
        if user_answer == "!exit":
            break

        context.append(Message(user, user_answer, tokenizer))
        while len(context) > config["max_context_messages_nb"]:
            context.pop()

        print(bot_prompt, end="")
        bot_answer = generate(
            model,
            bot_prompt,
            context,
            device,
            tokenizer,
            1024,
            config["sample_nucleus_size"],
        )
        print(bot_answer)
        context += ""
