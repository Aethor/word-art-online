import sys
import random
import os
import math

import torch
from tqdm.auto import tqdm
from transformers import AdamW, GPT2Tokenizer, CamembertTokenizer

from gna.gna.config import Config
from datas import Dataset, batches, batches_nb
from model import GPT2UserImpersonator, CamembertUserImpersonator


def train(
    model: torch.nn.Module,
    model_dir_path: str,
    dataset: Dataset,
    tokenizer,
    user: str,
    epochs_nb: int,
    batch_size: int,
    max_messages_nb: int,
    max_context_len: int,
    device: torch.device,
    optimizer,
    scheduler=None,
    cur_epoch: int = 0,
    cur_min_loss: float = math.inf,
) -> torch.nn.Module:
    model.train()
    model.to(device)

    loss_fn = torch.nn.CrossEntropyLoss()

    min_loss = cur_min_loss

    for epoch in range(cur_epoch, epochs_nb):

        mean_loss_list = []

        sequences = dataset.sequences(user, max_messages_nb, max_context_len)
        random.shuffle(sequences)
        batches_progress = tqdm(
            batches(sequences, tokenizer, batch_size),
            total=batches_nb(sequences, batch_size),
        )

        for batch in batches_progress:

            input_ids = batch[0].to(device)
            attention_mask = batch[1].to(device)
            tokens_type_ids = batch[2].to(device)
            target_ids = batch[3].to(device)

            optimizer.zero_grad()
            out = model(input_ids, attention_mask, tokens_type_ids)[0]
            loss = loss_fn(out.transpose(1, 2), target_ids)
            loss.backward()
            optimizer.step()

            batches_progress.set_description(f"[epoch : {epoch}][loss : {loss.item()}]")
            mean_loss_list.append(loss.item())

        epoch_mean_loss = math.inf
        if len(mean_loss_list) > 0:
            epoch_mean_loss = sum(mean_loss_list) / len(mean_loss_list)
            print(f"mean loss : {epoch_mean_loss}")

        if epoch_mean_loss < min_loss:
            print(
                f"epoch mean loss better than previous best loss ({min_loss}), saving model..."
            )
            torch.save(
                {
                    "epoch": epoch,
                    "model_state": model.state_dict(),
                    "optimizer_state": optimizer.state_dict(),
                    "min_loss": epoch_mean_loss,
                    "model_type": type(model).__name__,
                    "target_user": user,
                    "users": list(dataset.users.keys()),
                },
                model_dir_path + "/model.pth",
            )
            min_loss = epoch_mean_loss

    return model


if __name__ == "__main__":
    config = Config("./configs/definitions/train.json", sys.argv[1:])
    if config["transformer_model_type"] == "gpt2":
        model = GPT2UserImpersonator()
        tokenizer = GPT2Tokenizer.from_pretrained("gpt2")
        tokenizer.sep_token = tokenizer.eos_token
        tokenizer.pad_token = tokenizer.eos_token
    elif config["transformer_model_type"] == "camembert":
        model = CamembertUserImpersonator()
        tokenizer = CamembertTokenizer.from_pretrained("camembert-base")
    else:
        raise Exception(
            "transformer model type : {}".format(config["transformer_model_type"])
        )

    print(f"running with config :\n {str(config)}")

    model_dir_path = "./models/" + config["user"]
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    optimizer = AdamW(model.parameters(), lr=config["learning_rate"])

    cur_epoch = 0
    cur_min_loss = math.inf

    if not os.path.exists(model_dir_path):
        os.mkdir(model_dir_path)

    elif os.path.exists(model_dir_path + "/model.pth"):
        user_answer = None
        while user_answer != "r" and user_answer != "o":
            print(f"model for {model_dir_path} already exists")
            user_answer = input("r. Resume training\no. Overwrite\n(r/o) ")

        if user_answer == "r":
            print("resuming training...")
            model_checkpoint = torch.load(
                model_dir_path + "/model.pth", map_location=device
            )
            model.load_state_dict(model_checkpoint["model_state"], strict=False)
            optimizer.load_state_dict(model_checkpoint["optimizer_state"])
            cur_epoch = model_checkpoint["epoch"] + 1
            cur_min_loss = model_checkpoint["min_loss"]

        elif user_answer == "o":
            print("overwriting.")

    print(f"model save path is {model_dir_path}")

    train_dataset = Dataset(
        config["dataset_path"], tokenizer, config["datas_usage_ratio"]
    )

    model = train(
        model,
        model_dir_path,
        train_dataset,
        tokenizer,
        config["user"],
        config["epochs_nb"],
        config["batch_size"],
        config["max_messages_nb"],
        config["max_context_len"],
        device,
        optimizer,
        None,
        cur_epoch,
        cur_min_loss,
    )
