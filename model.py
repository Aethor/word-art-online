from typing import Optional

import torch
from transformers import GPT2Model, CamembertModel


class CamembertUserImpersonator(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.camembert = CamembertModel.from_pretrained("camembert-base")

        self.linear = torch.nn.Linear(
            self.camembert.config.hidden_size, self.camembert.config.vocab_size
        )

    def forward(
        self, input_ids: torch.Tensor, attention_mask: Optional[torch.Tensor], *args
    ) -> torch.Tensor:
        """
        :param input_ids:      (batch_size, seq_len)
        :param attention_mask: (batch_size, seq_len)
        :return: output_ids, past
            output_ids : (batch_size, seq_len, vocab_size)
        """
        # (batch_size, seq_len, hidden_size)
        out = self.camembert(input_ids, attention_mask=attention_mask)[0]
        # (batch_size, seq_len, vocab_size)
        return (self.linear(out),)


class GPT2UserImpersonator(torch.nn.Module):
    def __init__(self):
        super().__init__()
        self.gpt2 = GPT2Model.from_pretrained("gpt2")

        self.linear = torch.nn.Linear(
            self.gpt2.config.hidden_size, self.gpt2.config.vocab_size
        )

    def forward(
        self,
        input_ids: torch.Tensor,
        attention_mask: Optional[torch.Tensor],
        token_type_ids: torch.Tensor,
        past: Optional[torch.Tensor] = None,
    ) -> torch.Tensor:
        """
        :param input_ids:      (batch_size, seq_len)
        :param attention_mask: (batch_size, seq_len)
        :param past:           list(2, batch_size, heads_nb, seq_len, embed_size_per_head)(layers_nb)
        :return: output_ids, past
            ouput_ids : (batch_size, seq_len, vocab_size)
            past :      list(2, batch_size, heads_nb, seq_len, embed_size_per_head)(layers_nb)
        """
        # (batch_size, seq_len, hidden_size)
        out, past = self.gpt2(
            input_ids,
            past=past,
            attention_mask=attention_mask,
            token_type_ids=token_type_ids,
        )
        # (batch_size, seq_len, vocab_size)
        return self.linear(out), past
